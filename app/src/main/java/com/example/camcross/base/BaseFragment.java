package com.example.camcross.base;

import android.support.v4.app.Fragment;

public abstract class BaseFragment<T extends BasePresenter> extends Fragment implements BaseContract<T> {

    @Override
    public void showLoading() {

    }

    @Override
    public void showLoading(int resId) {

    }

    @Override
    public void dismissLoading() {

    }

    @Override
    public void showError(int resId) {

    }

    @Override
    public void showError(CharSequence message) {

    }

    @Override
    public void showToast(int resId) {

    }

    @Override
    public void showToast(CharSequence message) {

    }
}
