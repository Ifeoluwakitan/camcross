package com.example.camcross.base;

public interface BasePresenter {
    void start();
}
