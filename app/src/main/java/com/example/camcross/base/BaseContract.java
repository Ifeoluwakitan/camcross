package com.example.camcross.base;

import android.support.annotation.StringRes;

public interface BaseContract<T extends BasePresenter> {
    void showLoading();

    void showLoading(@StringRes int resId);

    void dismissLoading();

    void showError(@StringRes int resId);

    void showError(CharSequence message);

    void showToast(@StringRes int resId);

    void showToast(CharSequence message);
}
